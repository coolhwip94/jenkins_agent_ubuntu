# Ubuntu Jenkins Agent
> This repo will contain example of creating a jenkins agent with ubuntu. 


## Config
---
- ansible.cfg
  - defines defaults
- hosts file
  - define connection as docker
  > this allows ansible to communicate via docker instead of ssh
- Dockerfile
  - bare minimum required for docker container to be used with ansible


## Playbooks
- jenkins_agent_ubuntu.yml
  - this will create a jenkins agent within docker container
  - ssh will be enabled
  - jenkins_agent user will be created with root access
  - ssh private key will be copied into container, this will allow jenkins agent to use ansible for remote hosts if user is allowed

- jenkins_agent_role.yml
  - this does exactly the same as jenkins_agent_ubuntu.yml
  - this utilizes a role to setup the agent in the container
  - ssh key files should be put into the `roles/jenkins_agent/files` directory


- Variables 
  - `ssh_key_file`: public ssh key for jenkins_agent, default = `/home/ltruong/hwip_jenkins/jenkins_home/.ssh/jenkins_agent_key.pub`
  - `image_name`: name for image build, default = `ubuntu_jenkins_agent`
  - `jenkins_agent_user`: username to create, default = `jenkins_agent`
  - `mapped_port`: port to map ssh, default = `2023` (configured as 2023:22)
  - `container_name`: name for container, default = `ubuntu_jenkins_agent_1`
  - `packages`: array of packages to install, default = `[python3-pip, openssh-server]`
  - `ssh_key_private_file`: path to ssh private key, make sure to allow this on any host you may want to manage with ansible from jenkins agent

